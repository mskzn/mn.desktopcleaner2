﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Threading;

namespace MN.DesktopCleaner
{
    public class Program
    {
        static void Main()
        {
            //Open Main window-Form
            Application.Run(new FormMain());
        }

        public static void Save()
        {
            //Call collectionData.cs and count icons on the desktop. 
            collectIconData cid = new collectIconData();
            collectIconData.icondata icd;
            try
            {
                icd = cid.getIcondata(0);
            }
            catch (IndexOutOfRangeException e)
            {
                //there is no icon on the desktop.
                System.Diagnostics.Debug.WriteLine(e.Source);
            }
            int inticonnumber = cid.getIconNumber();

            //Call class and exceute funciton to get desktophight.
            MeasureDesktopArea mdh = new MeasureDesktopArea();
            int j = mdh.getdesktophight();
            System.Diagnostics.Debug.WriteLine("desktophight is " + j);
            int k = mdh.getdesktopwidth();
            System.Diagnostics.Debug.WriteLine("desktopwidth is " + k);

            MeasureDesktopArea myMeasureDesktopArea = new MeasureDesktopArea();
            int desktophight = myMeasureDesktopArea.getdesktophight();

            //get List of icondata and pass it to DeleteOrSave method.
            collectIconData collectIconData = new collectIconData();
            List<collectIconData.icondata> icondatas = collectIconData.geticondatacollection();
            DelteOrsave(icondatas, j);
        }

        static void DelteOrsave(List<FindIcons5.collectIconData.icondata> inputList, int hight)
        {
            //prepare the List in which icon data is saved.
            List<FindIcons5.collectIconData.icondata> icondatas = new List<collectIconData.icondata>();
            //for each item it it locates on upper half of desktop it is deleted,else it is saved.
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            foreach (FindIcons5.collectIconData.icondata icondata in inputList)
            {

                //save coordinates and name into list
                icondatas.Add(new collectIconData.icondata { name = icondata.name, x = icondata.x, y = icondata.y });
                //check if archeves-folder exists.
                if (Directory.Exists(path + @"\archeves"))
                {
                    //do nothing
                }
                else
                {
                    //create aechives folder
                    DirectoryInfo di = Directory.CreateDirectory(path + @"\archeves");
                }


                //move files to designated folder.
                MoveFiles.Movefile(icondata.name, path, path + @"\archeves");

            }
            //save list into JSON
            SaveinJSON.serializeDataset(icondatas);
            //move JSON file from desktop to archeve foler.
            MoveFiles.Movefile("myjsonfile.json", path, path + @"\archeves");
        }

        public static void Restore()
        {
            //************************************************************
            // copy files from archive folder to desktop

            //get the path of desktop.
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            //get files from archive folder.
            string[] myfiles = Directory.GetFiles(path + @"\archeves");
            List<string> restoredfiles = new List<string>();

            //Console.WriteLine(myfiles);
            //move files from archeve folder to desktop.

            foreach (string file in myfiles)
            {
                //remove path sentence from full-path.
                string fName = file.Substring(path.Length + 10);
                //Console.WriteLine(fName);
                //move
                if (fName.Equals("myjsonfile.json"))
                {
                    //do nothing
                }
                else
                {
                    //move
                    MoveFiles.Movefile(fName, path + @"\archeves", path);
                    //save restored file name in list
                    restoredfiles.Add(fName);
                }

            }


            Thread.Sleep(2000);

            //*************************************************************
            // get icondata list with index.

            collectIconData collectIconData = new collectIconData();
            List<collectIconData.icondatawindex> icondataswindex = collectIconData.geticondatacollectionwindex();


            //*************************************************************
            //read jsonfile to string

            path = path + @"\archeves\myjsonfile.json";
            List<collectIconData.icondata> icondata_saved = JsonConvert.DeserializeObject<List<collectIconData.icondata>>(File.ReadAllText(path));

            //compare icondata_restored with icondata_saved and collect coordinate data.

            foreach (string icondata_restored_filename in restoredfiles)
            {
                //check if same name exits in icondata_restored
                if (icondata_saved.Exists(x => x.name == icondata_restored_filename))
                {
                    collectIconData.icondata icondata = icondata_saved.Find(i => i.name == icondata_restored_filename);
                    //*********place file on specific location.******************************

                    //get index
                    collectIconData.icondatawindex icondatawindex = icondataswindex.Find(j => j.icondata.name == icondata.name);

                    //place file
                    PlaceFiles.placeFile(icondatawindex.index, icondata.x, icondata.y);

                }
            }

        }

    }
}
