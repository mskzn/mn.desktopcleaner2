﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace MN.DesktopCleaner2
{
    class MeasureDesktopArea
    {

        public int getdesktophight()
        {
            //the point is to make sure correct reference to resources are implemented(dll or so).
            //in oreder to make references correct projcet type is required.

            //measure desktop hight
            System.Drawing.Rectangle workingRectangle =System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            
            
            int screenhight = workingRectangle.Height;
            System.Diagnostics.Debug.WriteLine(screenhight);

            //if a icon locates on the upper side of desktop, code detect it.

            return screenhight;


        }

        public int getdesktopwidth()
        {
            //the point is to make sure correct reference to resources are implemented(dll or so).
            //in oreder to make references correct projcet type is required.

            //measure desktop width
            System.Drawing.Rectangle workingRectangle = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            int screenwidth = workingRectangle.Width;
            System.Diagnostics.Debug.WriteLine(screenwidth);

            //if a icon locates on the upper side of desktop, code detect it.

            return screenwidth;


        }

    }
}
