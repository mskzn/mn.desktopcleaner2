﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MN.DesktopCleaner2
{
    static class deleteNullvalue
    {
        public static byte[] getNoNullArray(byte[] inputarray)
        {

            int i = inputarray.Length - 1;
            while (inputarray[i] == 0)
                --i;
            byte[] bar = new byte[i + 2];
            Array.Copy(inputarray, bar, i + 2);
            
            return bar;

        }
    }

    static class clearArray
    {
        public static void inputZerointoArray(byte[] inputarray)
        {
            int i = inputarray.Length;

            for(int j=0;j<i;j++)
            {
                inputarray[j] = 0;
            }
            
        }


    }

}
