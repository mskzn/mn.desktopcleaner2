﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MN.DesktopCleaner2
{
    static class DeleteFiles
    {
        public static void Deletefile(string strPathtoFileorFolder)
        {
                       
            
            // Delete a file by using File class static method...
            if (System.IO.File.Exists(strPathtoFileorFolder))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(strPathtoFileorFolder);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
            }

            // ...or by using FileInfo instance method.
            System.IO.FileInfo fi = new System.IO.FileInfo(strPathtoFileorFolder);
            try
            {
                fi.Delete();
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
            }

            // Delete a directory. Must be writable or empty.
            try
            {
                System.IO.Directory.Delete(strPathtoFileorFolder);
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
            }
            // Delete a directory and all subdirectories with Directory static method...
            if (System.IO.Directory.Exists(strPathtoFileorFolder))
            {
                try
                {
                    System.IO.Directory.Delete(strPathtoFileorFolder, true);
                }

                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            // ...or with DirectoryInfo instance method.
            //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(@"C:\Users\Public\public");
            // Delete this dir and all subdirs.
            //try
            //{
            //    di.Delete(true);
            //}
            //catch (System.IO.IOException e)
            //{
            //    Console.WriteLine(e.Message);
            //}
        }
    }
}
