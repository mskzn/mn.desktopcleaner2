﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MN.DesktopCleaner2
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Program.Save();
        }

        private void buttonRestore_Click(object sender, EventArgs e)
        {
            Program.Restore();
        }
    }
}
