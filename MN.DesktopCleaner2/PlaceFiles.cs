﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;

namespace MN.DesktopCleaner2
{
    static public class PlaceFiles
    {

        public delegate bool CallBackPtr(IntPtr hwnd, int lParam);

        [DllImport("user32.DLL")]public static extern IntPtr FindWindow(string lpszClass, string lpszWindow);
        [DllImport("user32.DLL")]public static extern IntPtr FindWindowEx(IntPtr hwndParent,IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll", SetLastError = true)]public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")] static extern IntPtr GetShellWindow();
        [DllImport("user32.dll")] public static extern IntPtr EnumWindows(CallBackPtr callPtr, IntPtr lPar);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        public static IntPtr MakeLParam(int wLow, int wHigh)
        {
            return (IntPtr)(((short)wHigh << 16) | (wLow & 0xffff));
        }

        const uint LVM_SETITEMPOSITION = 0x1000 + 15;


        static public void placeFile(int index,int x,int y)
        {

            //get the handle of the desktop listview
            IntPtr _ProgMan = GetShellWindow();
            IntPtr _SHELLDLL_DefViewParent = _ProgMan;
            IntPtr _SHELLDLL_DefView = FindWindowEx(_ProgMan, IntPtr.Zero, "SHELLDLL_DefView", null);
            IntPtr _SysListView32 = FindWindowEx(_SHELLDLL_DefView, IntPtr.Zero, "SysListView32", "FolderView");



            EnumWindows((hwnd, lParam) =>
            {
                StringBuilder lpClassName = new StringBuilder(255);
                int intLength = GetClassName(hwnd, lpClassName, 255);
                StringBuilder sbWorkerW = new StringBuilder("WorkerW");

                if (collectIconData.CompareStringBuilders(lpClassName, sbWorkerW))
                {
                    //System.Diagnostics.Debug.WriteLine("hit!");

                    IntPtr child = FindWindowEx(hwnd, IntPtr.Zero, "SHELLDLL_DefView", null);
                    if (child != IntPtr.Zero)
                    {
                        _SHELLDLL_DefViewParent = hwnd;
                        _SHELLDLL_DefView = child;
                        _SysListView32 = FindWindowEx(child, IntPtr.Zero, "SysListView32", "FolderView");
                        return false;
                    }
                }
                return true;
            }, IntPtr.Zero);


            //get the number of icons on desktop
            collectIconData cid = new collectIconData();
            collectIconData.icondata icndt = new collectIconData.icondata();
            icndt = cid.getIcondata(0);
            int i = cid.getIconNumber();

        
                                   
            SendMessage(_SysListView32, LVM_SETITEMPOSITION, (IntPtr)index, MakeLParam(x, y));

        }



















    }
}
