﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Data;
using FastMember;

namespace MN.DesktopCleaner2
{
    static public class SaveinJSON
    {

        static public void saveinJSN()
        {

            Account account = new Account
            {
                Email = "james@example.com",
                Active = true,
                CreatedDate = new DateTime(2013, 1, 20, 0, 0, 0, DateTimeKind.Utc),
                Roles = new List<string>
                {
                    "User",
                    "Admin"
                }
            };
            string json = JsonConvert.SerializeObject(account, Formatting.Indented);
            // {
            //   "Email": "james@example.com",
            //   "Active": true,
            //   "CreatedDate": "2013-01-20T00:00:00Z",
            //   "Roles": [
            //     "User",
            //     "Admin"
            //   ]
            // }
            Console.WriteLine(json);
        }


        static public void saveinJSNtofile(string filename,int xcoordinate,int ycoordinate)
        {
            FileProperty fileproperty = new FileProperty();
            fileproperty.Name = filename;
            fileproperty.Xcoordinate = xcoordinate;
            fileproperty.Ycoordinate = ycoordinate;
            // serialize JSON directly to a file
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            using (StreamWriter file = File.CreateText(path+@"\fileproperty.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, fileproperty);
            }
        }

        static public void deserializefromFiletoJSON()
        {


            //get the path of desktop.
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            String str = File.ReadAllText(path + @"\myjsonfile.json");

            Console.WriteLine(str);

            // read json file into a JObject
            using (StreamReader file = File.OpenText(path+@"\myjsonfile.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject jobj = (JObject)JToken.ReadFrom(reader);
                //JArray jArray = JArray.Parse(jobj);

                

                Console.WriteLine(file);

               // Movie movie2 = (Movie)serializer.Deserialize(file, typeof(Movie));

               // System.Diagnostics.Debug.WriteLine(movie2.Name);
               // System.Diagnostics.Debug.WriteLine(movie2.Year);
            }
        }

        static public void serializeDataset()
        {
            DataSet dataSet = new DataSet("dataSet");
            dataSet.Namespace = "NetFrameWork";
            DataTable table = new DataTable();
            DataColumn idColumn = new DataColumn("id", typeof(int));
            idColumn.AutoIncrement = true;

            DataColumn filenameColumn = new DataColumn("filename");
            DataColumn xcoordinateColumn = new DataColumn("xcoordinate");
            DataColumn ycoordinateColumn = new DataColumn("ycoordinate");
            table.Columns.Add(idColumn);
            table.Columns.Add(filenameColumn);
            table.Columns.Add(xcoordinateColumn);
            table.Columns.Add(ycoordinateColumn);
            dataSet.Tables.Add(table);

            for (int i = 0; i <3 ; i++)
            {
                DataRow newRow = table.NewRow();
                newRow["filename"] = "item " + i;
                newRow["xcoordinate"] = 200;
                newRow["ycoordinate"] = 300;
                table.Rows.Add(newRow);
            }

            dataSet.AcceptChanges();

            string json = JsonConvert.SerializeObject(dataSet, Formatting.Indented);

            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            File.WriteAllText(path+@"\myfiles.json", JsonConvert.SerializeObject(json));

            Console.WriteLine(json);
            // {
            //   "Table1": [
            //     {
            //       "id": 0,
            //       "item": "item 0"
            //     },
            //     {
            //       "id": 1,
            //       "item": "item 1"
            //     }
            //   ]
            // }

        }

        static public void serializeDataset(List<MN.DesktopCleaner2.collectIconData.icondata> inputcollection)
        {
            
            try {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                path = path + @"\myjsonfile.json";
                File.WriteAllText(path, JsonConvert.SerializeObject(inputcollection));
                
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }

            Console.WriteLine(JsonConvert.SerializeObject(inputcollection));
        }

        static public List<MN.DesktopCleaner2.collectIconData.icondata> deserializeDataset(string json)
        {
            List<MN.DesktopCleaner2.collectIconData.icondata> icondatas=new List<collectIconData.icondata>();

            try { 
            icondatas = JsonConvert.DeserializeObject<List<MN.DesktopCleaner2.collectIconData.icondata>>(json);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return icondatas;
        }

    }


    public class Account
    {
        public string Email { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public IList<string> Roles { get; set; }
    }

    public class FileProperty
    {
        public string Name { get; set; }
        public int Xcoordinate { get; set; }
        public int Ycoordinate { get; set; }
    }

    public class Product
    {
        public string Name { get; set; }
        public string ExpiryDate { get; set; }
        public string Price { get; set; }
        public string Sizes { get; set; }

    }






}
