﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Data;

namespace MN.DesktopCleaner2
{
    public class collectIconData
    {
        public const uint LVM_FIRST = 0x1000;
        public const uint LVM_GETITEMCOUNT = LVM_FIRST + 4;
        public const uint LVM_GETITEMW = LVM_FIRST + 75;
        public const uint LVM_GETITEMTEXT = LVM_FIRST + 45;
        public const uint PROCESS_VM_OPERATION = 0x0008;
        public const uint PROCESS_VM_READ = 0x0010;
        public const uint PROCESS_VM_WRITE = 0x0020;
        public const uint MEM_COMMIT = 0x1000;
        public const uint MEM_RELEASE = 0x8000;
        public const uint MEM_RESERVE = 0x2000;
        public const uint PAGE_READWRITE = 4;
        public const int LVIF_TEXT = 0x0001;
        public const uint LVM_GETITEMPOSITION = LVM_FIRST + 16;

        public delegate bool CallBackPtr(IntPtr hwnd, int lParam);

        public struct LVITEM
        {
            public int mask;
            public int iItem;
            public int iSubItem;
            public int state;
            public int stateMask;
            public IntPtr pszText; // string
            public int cchTextMax;
            public int iImage;
            public IntPtr lParam;
            public int iIndent;
            public int iGroupId;
            public int cColumns;
            public IntPtr puColumns;
        }

        [DllImport("user32.dll")] public static extern IntPtr FindWindow(string lpszClass, string lpszWindow);
        [DllImport("user32.dll")] public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll")] static extern IntPtr GetShellWindow();
        [DllImport("user32.dll")] public static extern IntPtr EnumWindows(CallBackPtr callPtr, IntPtr lPar);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
        [DllImport("user32.DLL")] private static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);
        [DllImport("kernel32.dll")] public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int nSize, ref uint vNumberOfBytesRead);
        [DllImport("kernel32.dll", SetLastError = true)] public static extern int ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, IntPtr lpBuffer, int nSize, ref uint vNumberOfBytesRead);
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Ansi)] public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int nSize, ref uint vNumberOfBytesRead);
        [DllImport("user32.dll")] private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint dwProcessId);
        [DllImport("kernel32.dll")] private static extern IntPtr OpenProcess(uint dwDesiredAccess, bool bInheritHandle, uint dwProcessId);
        [DllImport("kernel32.dll")] private static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);


        public static int vItemCount;
        private static object myobj;


        public icondata getIcondata(int i)
        {
            //get the handle of the desktop listview
            IntPtr _ProgMan = GetShellWindow();
            IntPtr _SHELLDLL_DefViewParent = _ProgMan;
            IntPtr _SHELLDLL_DefView = FindWindowEx(_ProgMan, IntPtr.Zero, "SHELLDLL_DefView", null);
            IntPtr _SysListView32 = FindWindowEx(_SHELLDLL_DefView, IntPtr.Zero, "SysListView32", "FolderView");


            EnumWindows((hwnd, lParam) =>
            {
                StringBuilder lpClassName = new StringBuilder(255);
                int intLength = GetClassName(hwnd, lpClassName, 255);
                StringBuilder sbWorkerW = new StringBuilder("WorkerW");

                if (CompareStringBuilders(lpClassName, sbWorkerW))
                {
                    //System.Diagnostics.Debug.WriteLine("hit!");

                    IntPtr child = FindWindowEx(hwnd, IntPtr.Zero, "SHELLDLL_DefView", null);
                    if (child != IntPtr.Zero)
                    {
                        _SHELLDLL_DefViewParent = hwnd;
                        _SHELLDLL_DefView = child;
                        _SysListView32 = FindWindowEx(child, IntPtr.Zero, "SysListView32", "FolderView");
                        return false;
                    }
                }
                return true;
            }, IntPtr.Zero);


            //public int pListview= _SysListView32;

            vItemCount = SendMessage(_SysListView32, LVM_GETITEMCOUNT, 0, 0);

            uint vProcessId;
            GetWindowThreadProcessId(_SysListView32, out vProcessId);
            IntPtr vProcess = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ |
            PROCESS_VM_WRITE, false, vProcessId);

            IntPtr textPointer = VirtualAllocEx(vProcess, IntPtr.Zero, 4096, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
            IntPtr vPointer = VirtualAllocEx(vProcess, IntPtr.Zero, 4096, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

            icondata[] desktopicondata = new icondata[vItemCount];
            // var icondatas = new List<icondata>();

            for (int j = 0; j < vItemCount; j++)
            {
                byte[] vBuffer = new byte[256];
                LVITEM[] vItem = new LVITEM[2];
                // 0 item position
                vItem[0].mask = LVIF_TEXT;
                vItem[0].iItem = j;
                vItem[0].iSubItem = 0;
                vItem[0].cchTextMax = vBuffer.Length;
                vItem[0].pszText = (IntPtr)((int)vPointer + Marshal.SizeOf(typeof(LVITEM)));
                // 1 item name
                vItem[1].mask = LVIF_TEXT;
                vItem[1].iItem = j;
                vItem[1].iSubItem = 0;
                vItem[1].cchTextMax = vBuffer.Length;
                vItem[1].pszText = (IntPtr)((int)textPointer + Marshal.SizeOf(typeof(LVITEM)));

                uint vNumberOfBytesRead = 0;
                StringBuilder text = new StringBuilder(260);
                // System.Diagnostics.Debug.WriteLine("j is " + j);

                //Get item position **************************************************************************************************************
                WriteProcessMemory(vProcess, vPointer, Marshal.UnsafeAddrOfPinnedArrayElement(vItem, 0)
                                    , Marshal.SizeOf(typeof(LVITEM)), ref vNumberOfBytesRead);

                SendMessage(_SysListView32, LVM_GETITEMPOSITION, j, vPointer.ToInt32());

                Point[] vPoint = new Point[2];

                ReadProcessMemory(vProcess, vPointer, Marshal.UnsafeAddrOfPinnedArrayElement(vPoint, 0), Marshal.SizeOf(typeof(Point)), ref vNumberOfBytesRead);

                string IconLocation = vPoint[0].ToString();

                //System.Diagnostics.Debug.WriteLine("IconLocation is" + IconLocation);

                //Get item Name ********************************************************************************************************************

                WriteProcessMemory(vProcess, textPointer, Marshal.UnsafeAddrOfPinnedArrayElement(vItem, 1)
                    , Marshal.SizeOf(typeof(LVITEM)), ref vNumberOfBytesRead);

                SendMessage(_SysListView32, LVM_GETITEMW, 0, textPointer.ToInt32());

                clearArray.inputZerointoArray(vBuffer);

                //read files name
                ReadProcessMemory(vProcess, textPointer, vBuffer, vBuffer.Length, ref vNumberOfBytesRead);

                string vText = Encoding.Unicode.GetString(vBuffer, 72, vBuffer.Length - 72);
                byte[] wBuffer = deleteNullvalue.getNoNullArray(vBuffer);
                string wText = Encoding.Unicode.GetString(wBuffer, 72, wBuffer.Length - 72);
                string xText = Deletebackslash.gettruepartofFilename(wText);

                // string IconName = vPoint[1].ToString();
                // System.Diagnostics.Debug.WriteLine(vProcess);
                // System.Diagnostics.Debug.WriteLine(vPointer);
                //System.Diagnostics.Debug.WriteLine("Iconname is " + vText);
                //System.Diagnostics.Debug.WriteLine(Environment.NewLine);
                //Console.WriteLine(vText);

                desktopicondata[j].x = vPoint[0].X;
                desktopicondata[j].y = vPoint[0].Y;
                desktopicondata[j].name = xText;

            }

            try
            {
                return desktopicondata[i];
            } catch (IndexOutOfRangeException e)
            {
                //return exception to parent method.
                throw;
            }
        }

        public int getIconNumber()
        {
            return vItemCount;
        }

        public List<icondata> geticondatacollection()
        {
            //run getIcondata
            collectIconData collectIconData = new collectIconData();
            collectIconData.getIcondata(0);

            //get number of icons
            int numberoficons = vItemCount;

            //declare collection
            var icondatas = new List<icondata>();

            //with for loop get each icondata and save them into collection.
            for (int icounter = 0; icounter < numberoficons; icounter++)
            {
                icondatas.Add(collectIconData.getIcondata(icounter));
            }

            //return value
            return icondatas;
        }

        public List<icondatawindex> geticondatacollectionwindex()
        {
            //run getIcondata
            collectIconData collectIconData = new collectIconData();
            collectIconData.getIcondata(0);

            //get number of icons
            int numberoficons = vItemCount;

            //declare collection
            var icondatas = new List<icondatawindex>();

            //with for loop get each icondata and save them into collection.
            for (int icounter = 0; icounter < numberoficons; icounter++)
            {
                icondatawindex i = new icondatawindex();
                i.icondata = collectIconData.getIcondata(icounter);
                i.index = icounter;                               
                icondatas.Add(i);
            }

            //return value
            return icondatas;
        }


        public static bool CompareStringBuilders(StringBuilder stb1, StringBuilder stb2)
        {
            bool bcompareStringBuilders = true;

            //compare lengths of each stringbuilders.
            if (stb1.Length == stb2.Length)
            {
                for (int i = 0; i < stb1.Length; i++)
                {
                    if (stb1[i] == stb2[i])
                    {
                        //check next char
                    }
                    else
                    {
                        bcompareStringBuilders = false;
                    }
                }
            }
            else
            {
                bcompareStringBuilders = false;
            }
            return bcompareStringBuilders;
        }

        public struct icondata
        {
            public int x;
            public int y;
            public string name;
        }

        public struct icondatawindex
        {
            public icondata icondata;
            public int index;
        }

    }




}
